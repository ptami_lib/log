package log

import (
	"encoding/json"
	"fmt"
)

type Logger struct {
}

func Start(format string, a ...interface{}) {
	message := fmt.Sprintf(format, a...)
	fmt.Printf("\033[92m%s\033[0m", message)
	fmt.Println()
}

func Info(format string, a ...interface{}) {
	fmt.Printf(format, a...)
	fmt.Println()
}

func Error(format string, a ...interface{}) {
	message := fmt.Sprintf(format, a...)
	fmt.Printf("\033[91m%s\033[0m", message)
	fmt.Println()
}

func Debug(format string, a ...interface{}) {
	message := fmt.Sprintf(format, a...)
	fmt.Printf("\033[93m%s\033[0m", message)
	fmt.Println()
}

func StructToString(s interface{}) string {
	bytes, err := json.Marshal(s)
	if nil != err {
		return ""
	}

	return string(bytes)
}

func NewLogger() *Logger {
	return &Logger{}
}

func (l Logger) Start(format string, a ...interface{}) {
	message := fmt.Sprintf(format, a...)
	fmt.Printf("\033[92m%s\033[0m", message)
	fmt.Println()
}

func (l Logger) Info(format string, a ...interface{}) {
	fmt.Printf(format, a...)
	fmt.Println()
}

func (l Logger) Error(format string, a ...interface{}) {
	message := fmt.Sprintf(format, a...)
	fmt.Printf("\033[91m%s\033[0m", message)
	fmt.Println()
}

func (l Logger) Debug(format string, a ...interface{}) {
	message := fmt.Sprintf(format, a...)
	fmt.Printf("\033[93m%s\033[0m", message)
	fmt.Println()
}
