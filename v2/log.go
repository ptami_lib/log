package log

import (
	"encoding/json"
	"fmt"
	"gitlab.com/ptami_lib/util"
	"strconv"
)

func StartJson(logId string, title string, data interface{}) {
	Start(util.StructToString(buildJson(logId, title, "START", data)))
}

func Start(format string, a ...interface{}) {
	message := fmt.Sprintf(format, a...)
	fmt.Println(message)
}

func InfoJson(logId string, title string, data interface{}) {
	Info(util.StructToString(buildJson(logId, title, "INFO", data)))
}

func Info(format string, a ...interface{}) {
	message := fmt.Sprintf(format, a...)
	fmt.Println(message)
}

func ErrorJson(logId string, title string, data interface{}) {
	Error(util.StructToString(buildJson(logId, title, "ERROR", data)))
}

func Error(format string, a ...interface{}) {
	message := fmt.Sprintf(format, a...)
	fmt.Println(message)
}

func DebugJson(logId string, title string, data interface{}) {
	Debug(util.StructToString(buildJson(logId, title, "DEBUG", data)))
}

func Debug(format string, a ...interface{}) {
	message := fmt.Sprintf(format, a...)
	fmt.Println(message)
}

func buildJson(logId string, title string, level string, data interface{}) (dataMap map[string]interface{}) {
	var _err error

	dataMap = map[string]interface{}{
		"logId": logId,
		"title": title,
		"level": level,
	}

	switch data.(type) {
	case nil:
	case map[string]interface{}:
		dataMap["message"] = map[string]interface{}{}

		for k, v := range data.(map[string]interface{}) {
			dataMap["message"].(map[string]interface{})[k] = v
		}
	case string:
		dataMap["message"] = data.(string)
	case bool:
		dataMap["message"] = strconv.FormatBool(data.(bool))
	default:
		var dataBytes []byte
		mapFromData := map[string]interface{}{}

		dataBytes, _err = json.Marshal(data)
		if nil != _err {
			dataMap["message"] = fmt.Sprintf("error while marshaling (%v):%s", data, _err.Error())
			return
		}

		_err = json.Unmarshal(dataBytes, &mapFromData)
		if nil != _err {
			dataMap["message"] = fmt.Sprintf("error while unmarshaling (%s):%s", string(dataBytes), _err.Error())
			return
		}

		dataMap["message"] = map[string]interface{}{}

		for k, v := range mapFromData {
			dataMap["message"].(map[string]interface{})[k] = v
		}
	}

	return
}
